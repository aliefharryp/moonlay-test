package config

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

func OpenConn() error {
	dsn := "host=localhost user=postgres password=123456 dbname=moonlay3 port=5432 sslmode=disable TimeZone=Asia/Shanghai"
	var err error

	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		return err
	}
	return err

}

// func Close() error {
// 	return DB.Close()
// }
