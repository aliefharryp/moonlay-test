package models

type Todolist struct {
	Id_todolist    uint64         `gorm:"primaryKey"`
	Title_todolist string         `gorm:type:varchar;not null`
	Desc_todolist  string         `gorm:type:varchar;not null`
	File_todolist  string         `gorm:type:text`
	Subtodolist    []Sub_todolist `gorm:"foreignKey:id_todolist;"`
}

// type Todolist2 struct {
// 	Sub_todolistss []Sub_todolist `gorm:"foreignKey:TodolistID;"`
// }

// func (Todolist2) TableName() string {
// 	return "tb_todolist"
// }

func (Todolist) TableName() string {
	return "tb_todolist"
}

type Sub_todolist struct {
	Id_sub_todolist    uint64 `gorm:"primaryKey"`
	Title_sub_todolist string `gorm:type:varchar;not null`
	Desc_sub_todolist  string `gorm:type:varchar;not null`
	File_sub_todolist  string `gorm:type:text`
	Id_todolist        uint64
}

func (Sub_todolist) TableName() string {
	return "tb_sub_todolist"
}
