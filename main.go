package main

import (
	"io"
	"log"
	"moonly-test/config"
	"moonly-test/models"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/labstack/echo"
)

func main() {

	if err := config.OpenConn(); err != nil {
		log.Fatal("Connection Failed", err)
	}

	config.DB.AutoMigrate(&models.Todolist{}, &models.Sub_todolist{})

	e := echo.New()
	e.GET("/todolist", getTodolist)
	e.GET("/getAlllist", getAlllist)
	// e.GET("/subtodolist", getAlllist)
	e.GET("/subtodolist/:id", getSubtodolist)
	e.POST("/postTodolist", postTodolist)
	e.POST("/postSubtodolist", postSubtodolist)
	e.DELETE("/deleteTodolist", deleteTodolist)
	e.DELETE("/deleteSubtodolist", deleteSubtodolist)
	e.Logger.Fatal(e.Start(":1323"))

}

func getTodolist(c echo.Context) error {

	var tb_todolist []models.Todolist

	result := config.DB.Find(&tb_todolist)
	if result.Error != nil {
		return result.Error
	}

	return c.JSON(http.StatusOK, map[string][]models.Todolist{
		"data": tb_todolist,
	})
}

func getAlllist(c echo.Context) error {

	var tb_todolist2 []models.Todolist

	result := config.DB.Preload("Subtodolist").Find(&tb_todolist2)
	if result.Error != nil {
		return result.Error
	}

	return c.JSON(http.StatusOK, tb_todolist2)
}

func getSubtodolist(c echo.Context) error {

	id := c.Param("id")
	var subtodolist []models.Sub_todolist

	result := config.DB.First(&subtodolist, id)
	if result.Error != nil {
		return result.Error
	}

	return c.JSON(http.StatusOK, map[string][]models.Sub_todolist{
		"data": subtodolist,
	})
}

func postTodolist(c echo.Context) error {

	id := c.FormValue("id_todolist")
	title := c.FormValue("title_todolist")
	desc := c.FormValue("desc_todolist")

	file, err := c.FormFile("file_todolist")
	if err != nil {
		return err
	}
	splitName := strings.Split(file.Filename, ".")
	ext := splitName[len(splitName)-1]
	if ext == "pdf" || ext == "txt" {
		src, err := file.Open()
		if err != nil {
			return err
		}
		defer src.Close()

		dst, err := os.Create("attachment/" + file.Filename)
		if err != nil {
			return err
		}

		defer dst.Close()

		if _, err = io.Copy(dst, src); err != nil {
			return err
		}
	} else {
		return c.String(http.StatusOK, "Your file must be pdf or txt!")
	}

	if id != "" {
		idV, err := strconv.ParseUint(id, 10, 64)
		if err != nil {
			panic(err)
		}

		var todolist_edit = models.Todolist{
			Id_todolist:    idV,
			Title_todolist: title,
			Desc_todolist:  desc,
			File_todolist:  file.Filename,
		}

		result := config.DB.Model(&todolist_edit).Updates(todolist_edit)
		if result.Error != nil {
			return result.Error
		}

	} else {

		var todolist_add = models.Todolist{
			Title_todolist: title,
			Desc_todolist:  desc,
			File_todolist:  file.Filename,
		}

		result := config.DB.Create(&todolist_add)
		if result.Error != nil {
			return result.Error
		}
	}

	return c.String(http.StatusOK, "Success Create Data!")
}

func postSubtodolist(c echo.Context) error {
	id := c.FormValue("id_sub_todolist")
	title := c.FormValue("title_sub_todolist")
	desc := c.FormValue("desc_sub_todolist")
	id_todolist := c.FormValue("id_todolist")
	idTodo, err := strconv.ParseUint(id_todolist, 10, 64)
	if err != nil {
		panic(err)
	}

	file, err := c.FormFile("file_sub_todolist")
	if err != nil {
		return err
	}
	splitName := strings.Split(file.Filename, ".")
	ext := splitName[len(splitName)-1]
	if ext == "pdf" || ext == "txt" {
		src, err := file.Open()
		if err != nil {
			return err
		}
		defer src.Close()

		dst, err := os.Create("attachment/" + file.Filename)
		if err != nil {
			return err
		}

		defer dst.Close()

		if _, err = io.Copy(dst, src); err != nil {
			return err
		}
	} else {
		return c.String(http.StatusOK, "Your file must be pdf or txt!")
	}

	if id != "" {
		idV, err := strconv.ParseUint(id, 10, 64)
		if err != nil {
			panic(err)
		}
		var subtodolist_edit = models.Sub_todolist{
			Id_sub_todolist:    idV,
			Title_sub_todolist: title,
			Desc_sub_todolist:  desc,
			File_sub_todolist:  file.Filename,
			Id_todolist:        idTodo,
		}

		result := config.DB.Model(&subtodolist_edit).Updates(subtodolist_edit)
		if result.Error != nil {
			return result.Error
		}
	} else {
		var subtodolist_add = models.Sub_todolist{
			Title_sub_todolist: title,
			Desc_sub_todolist:  desc,
			File_sub_todolist:  file.Filename,
			Id_todolist:        idTodo,
		}

		result := config.DB.Create(&subtodolist_add)
		if result.Error != nil {
			return result.Error
		}
	}

	return c.String(http.StatusOK, "Success Create Data!")
}

func deleteTodolist(c echo.Context) error {
	id := c.FormValue("id_todolist")

	var todolist_del []models.Todolist

	result := config.DB.Delete(&todolist_del, id)
	if result.Error != nil {
		return result.Error
	}

	return c.String(http.StatusOK, "Success Delete Data!")

}

func deleteSubtodolist(c echo.Context) error {
	id := c.FormValue("id_sub_todolist")

	var subtodolist_del []models.Sub_todolist

	result := config.DB.Delete(&subtodolist_del, id)
	if result.Error != nil {
		return result.Error
	}

	return c.String(http.StatusOK, "Success Delete Data!")

}
